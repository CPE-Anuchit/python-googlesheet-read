# ติดตั้ง libraries
# pip install gspread
# pip install --upgrade oauth2client

# เรียกใช้งาน ServiceAccountCredentials
from oauth2client.service_account import ServiceAccountCredentials

# เรียกใช้งาน gspread
import gspread

# กำหนด scope ของไฟล์ข้อมูลที่เราใช้ว่าเป็นประเภทใด เช่น read-only (อ่านอย่างเดียว) หรือ read-and-write (อ่านและเขียน)
# กรณีอ่านอย่างเดียวใช้ Url = https://www.googleapis.com/auth/spreadsheets.readonly
# กรณีอ่านและเขียนใช้ Url = https://www.googleapis.com/auth/spreadsheets
scope = [
    'https://www.googleapis.com/auth/spreadsheets.readonly',
]

# กำหนด credentials ที่ได้จากการสร้าง Credentials จาก Google APIs & Service ซึงจะใช้ไฟล์ json
credentials = ServiceAccountCredentials.from_json_keyfile_name("credentials.json", scopes=scope)

# กำหนด gs เพื่อเข้าใช้งาน Google Sheet
gs = gspread.authorize(credentials=credentials)

# กำหนด sheet เพื่อเข้าถึง sheet โดยใช้ Url Google sheet  ที่ต้องการใช้งาน
sheet = gs.open_by_url("https://docs.google.com/spreadsheets/d/14ub1UhFkkldY9qZQ7v4y48ytAN655q2F7P3vQ4F23gE/edit#gid=0")

# กำหนด worksheet เพื่อเลือกเปิด sheet ที่ต้องการใช้งาน
worksheet = sheet.get_worksheet(0)

# แสดงข้อมูลที่อยู่ใน sheet ทั้งหมด
print(worksheet.get_all_values())